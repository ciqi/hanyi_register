import { createApp } from 'vue'

import App from './App.vue'
import router from "../src/router/index.js";
import store from "../src/store/index.js";
// import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import '../src/assets/less/index.less'
import 'element-plus/theme-chalk/el-message.css';

import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(store)
app.use(router).mount('#app')


