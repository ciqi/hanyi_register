import {createRouter,createWebHashHistory} from 'vue-router'

const  routes=[
    {
        path:'/',
        name: 'login',
        component:()=>import('../login/login.vue')
    },
    {
        path: '/main',
        name:'main',
        component:()=>import('../views/main.vue'),
        children:[
            {
                path: '/application',
                name: 'application',
                component: () => import('../views/application.vue')
            },
            {
                path: '/Overview',
                name: 'Overview',
                component:()=>import('../views/Overview.vue')
            },
            {
                path: '/Audit',
                name: 'Audit',
                component:()=>import('../views/Audit.vue')
            },
            {
                path: '/batch',
                name: 'batch',
                component:()=>import('../views/batch.vue')
            },
            {
                path: '/manage',
                name: 'manage',
                component:()=>import('../views/manage.vue')
            },
            {
                path: '/style_add',
                name: 'style',
                component:()=>import('../views/style.vue')
            },
            {
                path: '/relieve',
                name: 'relieve',
                component:()=>import('../views/relieve.vue')
            },
            {
                path: '/Whitelist',
                name: 'Whitelist',
                component:()=>import('../views/Whitelist.vue')
            }



        ]
    },
    {
        path: '/student',
        name:'student',
        component:()=>import('../Student/main.vue'),
        children:[
            {
                path:'/appli',
                name:'appli',
                component:()=>import("../Student/appli.vue")
            },
            {
                path: '/Over',
                name: 'Over',
                component: () => import('../Student/Over.vue')
            }


        ]
    },
    {
        path: '/zhuche',
        name:'zhuche',
        component:()=>import("../Student/logininsert.vue")
    }

]

const  router=createRouter({
    history:createWebHashHistory(),
    routes
})

export default router