
import {createStore} from 'vuex'

//创建store 实例
const store=createStore({
    state(){ //存储的单一状态，是存储基本数据
        return{
            count:10,
            dialogFormVisible:false ,
            dialogFormVisible_1:false ,
            dialogFormVisible_2:false ,
            update:'',
            update_1:'',
            update_2:'',
            type:0
        }
    },
    mutations:{
        increment(state){  //state表示 上面定义的state对象
            state.count++
        },
        updateDialogFormVisible(state){
            state.dialogFormVisible=!state.dialogFormVisible
        },
        updateDialogFormVisible_1(state){
            state.dialogFormVisible_1=!state.dialogFormVisible_1
        },
        updateDialogFormVisible_2(state){
            state.dialogFormVisible_2=!state.dialogFormVisible_2
        },
        update(state,value){
            state.update=value;
        },
        update_1(state,value){
            state.update_1=value;
        },
        update_2(state,value){
            state.update_2=value;
        },
        updateType(state,value){
            console.log(value,"<<>>>")
            state.type=value
            console.log(state.type,"<<1111>>>")
        }
    }
})

export default store